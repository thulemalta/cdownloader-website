from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from core.forms.register import Register
from core.forms.login import Login
from django.contrib.auth import authenticate, login, logout
from django.core.mail import send_mail
from contentdownloader import settings
from members.models import UserMeta
import random


def show_login(request):
    e = ""
    if request.method == 'POST':
        form = Login(request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect('/login_success/')
                else:
                    e = "Disabled or banned account."
            else:
                e = "Login and password doesn't match."
    else:
        form = Login()

    return render(request, 'login.html', {'form': form, 'error': e})


def show_register(request):
    if request.method == 'POST':
        form = Register(request.POST)
        if form.is_valid():

            subject = 'Inscription to content downloader';
            usr = form.cleaned_data['username']
            email = form.cleaned_data['email']
            pwd = random.randint(10000000, 99999999)
            msg = """
            Hi """ + usr + """,

            Thanks for registering to content downloader online platform !
            Here is your password : """ + str(pwd) + """

            I hope you will like our services.
            Enjoy !

            - ThuleMalta, content downloader founder.
            """

            if not User.objects.get(username=usr):
                user = User.objects.create_user(usr, email, pwd)
                usermeta = UserMeta(user=user, username=usr, bio="Another content downloader user")
                send_mail(subject, msg, settings.EMAIL_HOST_USER, [email], fail_silently=not settings.DEBUG)
                return HttpResponseRedirect('/thanks/')

            else:
                return HttpResponseRedirect('/already_exists/')

    else:
        form = Register()


    return render(request, 'register.html', {'form': form })


@login_required
def show_logout(request):
    logout(request)
    return render(request, 'logout_success.html')

def show_already_exists(request):
    logout(request)
    return render(request, 'already_exists.html')