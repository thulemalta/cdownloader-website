from django.shortcuts import render
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect

# Shows home
def home(request):
    if not User.is_anonymous:
        return HttpResponseRedirect('/cstore/')
    else:
        return render(request, 'home.html')

# Shows login
def login(request):
    return render(request, 'login.html')

# Shows register
def register(request):
    return render(request, 'register.html')

# Shows register
def thanks(request):
    return render(request, 'thanks.html')

# Shows register
def login_success(request):
    return render(request, 'login_success.html')