from django import forms


class Register(forms.Form):
    username = forms.CharField(label='username', max_length=32, min_length=3, widget=forms.TextInput(attrs={
        'placeholder': 'Login',
        'class': 'uk-width-1-1 uk-form-large'}))

    email = forms.EmailField(label='email', max_length=32, min_length=6, widget=forms.TextInput(attrs={
        'placeholder': 'Email',
        'class': 'uk-width-1-1 uk-form-large'}))