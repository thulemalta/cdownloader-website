from django import forms


class Login(forms.Form):
    username = forms.CharField(label='username', max_length=32, min_length=3, widget=forms.TextInput(attrs={
        'placeholder': 'Login',
        'class': 'uk-width-1-1 uk-form-large'}))

    password = forms.CharField(label='password', max_length=32, min_length=6, widget=forms.TextInput(attrs={
        'placeholder': 'Password',
        'class': 'uk-width-1-1 uk-form-large',
        'type': 'password'}))
