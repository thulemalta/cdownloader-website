# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_contenttype_enabled'),
    ]

    operations = [
        migrations.CreateModel(
            name='DownloadableContent',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64, verbose_name='Name')),
                ('slug', models.SlugField(max_length=64, verbose_name='Slug')),
                ('file', models.FileField(upload_to='/contents/09/01')),
                ('icon', models.ImageField(max_length=256, verbose_name='Icon', upload_to='')),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='Creation date')),
                ('enabled', models.BooleanField(default=False, verbose_name='Activated')),
            ],
        ),
        migrations.AlterField(
            model_name='contenttype',
            name='enabled',
            field=models.BooleanField(default=False, verbose_name='Activated'),
        ),
        migrations.AddField(
            model_name='downloadablecontent',
            name='categories',
            field=models.ForeignKey(to='core.ContentType'),
        ),
    ]
