# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_auto_20150913_1259'),
    ]

    operations = [
        migrations.AddField(
            model_name='contentcategory',
            name='enabled',
            field=models.BooleanField(default=False, verbose_name='Activated'),
        ),
        migrations.AlterField(
            model_name='contentcategory',
            name='banner',
            field=models.ImageField(upload_to='contentcategory/banner', verbose_name='Banner'),
        ),
        migrations.AlterField(
            model_name='contentcategory',
            name='icon',
            field=models.ImageField(upload_to='contentcategory/icon', verbose_name='Icon (96x96)'),
        ),
        migrations.AlterField(
            model_name='contenttype',
            name='banner',
            field=models.ImageField(upload_to='contenttype/banner', verbose_name='Banner'),
        ),
        migrations.AlterField(
            model_name='contenttype',
            name='icon',
            field=models.ImageField(upload_to='contenttype/icon', verbose_name='Icon (96x96)'),
        ),
        migrations.AlterField(
            model_name='downloadablecontent',
            name='banner',
            field=models.ImageField(upload_to='downloadablecontent/banner', verbose_name='Banner'),
        ),
        migrations.AlterField(
            model_name='downloadablecontent',
            name='icon',
            field=models.ImageField(upload_to='downloadablecontent/icon', verbose_name='Icon (96x96)'),
        ),
    ]
