# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0010_auto_20150916_1226'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contenttype',
            name='script_url',
        ),
    ]
