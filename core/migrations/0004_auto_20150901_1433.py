# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20150901_1429'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contenttype',
            name='icon',
            field=models.ImageField(verbose_name='Icon', upload_to=''),
        ),
        migrations.AlterField(
            model_name='downloadablecontent',
            name='file',
            field=models.URLField(verbose_name='Content address', max_length=256),
        ),
        migrations.AlterField(
            model_name='downloadablecontent',
            name='icon',
            field=models.ImageField(verbose_name='Icon', upload_to=''),
        ),
    ]
