# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_auto_20150901_1433'),
    ]

    operations = [
        migrations.AddField(
            model_name='downloadablecontent',
            name='author',
            field=models.CharField(default='Anonymous', max_length=64, verbose_name='Author'),
            preserve_default=False,
        ),
    ]
