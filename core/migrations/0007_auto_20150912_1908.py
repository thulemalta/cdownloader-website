# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_auto_20150912_1851'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='downloadablecontentversion',
            name='banner',
        ),
        migrations.RemoveField(
            model_name='downloadablecontentversion',
            name='categories',
        ),
        migrations.RemoveField(
            model_name='downloadablecontentversion',
            name='icon',
        ),
        migrations.RemoveField(
            model_name='downloadablecontentversion',
            name='website',
        ),
        migrations.AddField(
            model_name='downloadablecontentversion',
            name='content_url',
            field=models.URLField(verbose_name='Content URL', max_length=256, default='https://www.google.com'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='downloadablecontentversion',
            name='ctype',
            field=models.ForeignKey(verbose_name='Content type', default=0, to='core.DownloadableContent'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='contentcategory',
            name='icon',
            field=models.ImageField(verbose_name='Icon (96x96)', upload_to=''),
        ),
        migrations.AlterField(
            model_name='contenttype',
            name='categories',
            field=models.ForeignKey(verbose_name='Content category', to='core.ContentCategory'),
        ),
        migrations.AlterField(
            model_name='contenttype',
            name='icon',
            field=models.ImageField(verbose_name='Icon (96x96)', upload_to=''),
        ),
        migrations.AlterField(
            model_name='downloadablecontent',
            name='categories',
            field=models.ForeignKey(verbose_name='Content type', to='core.ContentType'),
        ),
        migrations.AlterField(
            model_name='downloadablecontent',
            name='icon',
            field=models.ImageField(verbose_name='Icon (96x96)', upload_to=''),
        ),
    ]
