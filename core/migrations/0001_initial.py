# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ContentCategory',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.CharField(max_length=64, verbose_name='Name')),
                ('slug', models.SlugField(max_length=64, verbose_name='Slug')),
            ],
            options={
                'verbose_name_plural': 'Content categories',
            },
        ),
        migrations.CreateModel(
            name='ContentType',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('name', models.CharField(max_length=64, verbose_name='Name')),
                ('slug', models.SlugField(max_length=64, verbose_name='Slug')),
                ('script_url', models.URLField(max_length=256, verbose_name='Script URL')),
                ('icon', models.ImageField(max_length=256, upload_to='', verbose_name='Icon')),
                ('date', models.DateTimeField(auto_now_add=True, verbose_name='Creation date')),
                ('categories', models.ForeignKey(to='core.ContentCategory')),
            ],
        ),
    ]
