# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_downloadablecontent_author'),
    ]

    operations = [
        migrations.CreateModel(
            name='DownloadableContentVersion',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('version', models.CharField(verbose_name='Version (verbose)', max_length=64)),
                ('slug', models.SlugField(unique=True, verbose_name='Version (slug, must be unique)', max_length=64)),
                ('website', models.URLField(verbose_name='Content website', max_length=256)),
                ('icon', models.ImageField(verbose_name='Icon', upload_to='')),
                ('banner', models.ImageField(verbose_name='Banner', upload_to='')),
                ('date', models.DateTimeField(verbose_name='Creation date', auto_now_add=True)),
                ('enabled', models.BooleanField(default=False, verbose_name='Activated')),
            ],
        ),
        migrations.RemoveField(
            model_name='downloadablecontent',
            name='file',
        ),
        migrations.AddField(
            model_name='contentcategory',
            name='banner',
            field=models.ImageField(default='http://placehold.it/800x600', verbose_name='Banner', upload_to=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='contentcategory',
            name='date',
            field=models.DateTimeField(default=datetime.datetime(2015, 9, 12, 16, 50, 27, 780978, tzinfo=utc), verbose_name='Creation date', auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='contentcategory',
            name='icon',
            field=models.ImageField(default='http://placehold.it/48x48', verbose_name='Icon', upload_to=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='contenttype',
            name='banner',
            field=models.ImageField(default='http://placehold.it/800x600', verbose_name='Banner', upload_to=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='contenttype',
            name='website',
            field=models.URLField(default='https://google.com', verbose_name='Content type website', max_length=256),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='downloadablecontent',
            name='banner',
            field=models.ImageField(default='http://placehold.it/800x600', verbose_name='Banner', upload_to=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='downloadablecontent',
            name='website',
            field=models.URLField(default='https://google.com', verbose_name='Content website', max_length=256),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='contenttype',
            name='slug',
            field=models.SlugField(unique=True, verbose_name='Slug', max_length=64),
        ),
        migrations.AlterField(
            model_name='downloadablecontent',
            name='slug',
            field=models.SlugField(unique=True, verbose_name='Slug', max_length=64),
        ),
        migrations.AddField(
            model_name='downloadablecontentversion',
            name='categories',
            field=models.ForeignKey(to='core.ContentType'),
        ),
    ]
