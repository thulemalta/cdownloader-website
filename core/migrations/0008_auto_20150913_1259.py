# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_auto_20150912_1908'),
    ]

    operations = [
        migrations.RenameField(
            model_name='downloadablecontent',
            old_name='categories',
            new_name='ctype',
        ),
        migrations.AddField(
            model_name='contentcategory',
            name='description',
            field=models.TextField(default='No description.', verbose_name='Description', max_length=512),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='contenttype',
            name='description',
            field=models.TextField(default='No description.', verbose_name='Description', max_length=512),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='downloadablecontent',
            name='description',
            field=models.TextField(default='No description.', verbose_name='Description', max_length=512),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='downloadablecontentversion',
            name='ctype',
            field=models.ForeignKey(verbose_name='Content', to='core.DownloadableContent'),
        ),
        migrations.AlterField(
            model_name='downloadablecontentversion',
            name='slug',
            field=models.SlugField(unique=True, verbose_name='Version (slug)', max_length=64),
        ),
    ]
