# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0009_auto_20150913_1651'),
    ]

    operations = [
        migrations.RenameField(
            model_name='contenttype',
            old_name='categories',
            new_name='category',
        ),
    ]
