# coding: utf-8
from django.db import models


class ContentCategory(models.Model):
    name = models.CharField(max_length=64, verbose_name="Name")
    slug = models.SlugField(max_length=64, verbose_name="Slug")
    description = models.TextField(max_length=512, verbose_name="Description")
    icon = models.ImageField(verbose_name="Icon (96x96)", upload_to="contentcategory/icon")
    banner = models.ImageField(verbose_name="Banner", upload_to="contentcategory/banner")
    date = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name="Creation date")
    enabled = models.BooleanField(default=False, verbose_name="Activated")

    class Meta:
        verbose_name_plural = "Content categories"

    def __str__(self):
        return self.name


class ContentType(models.Model):
    name = models.CharField(max_length=64, verbose_name="Name")
    slug = models.SlugField(max_length=64, verbose_name="Slug", unique=True)
    category = models.ForeignKey(ContentCategory, verbose_name="Content category")
    description = models.TextField(max_length=512, verbose_name="Description")
    website = models.URLField(verbose_name="Content type website", max_length=256)
    icon = models.ImageField(verbose_name="Icon (96x96)", upload_to="contenttype/icon")
    banner = models.ImageField(verbose_name="Banner", upload_to="contenttype/banner")
    date = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name="Creation date")
    enabled = models.BooleanField(default=False, verbose_name="Activated")

    def __str__(self):
        return self.name


class DownloadableContent(models.Model):
    name = models.CharField(max_length=64, verbose_name="Name")
    author = models.CharField(max_length=64, verbose_name="Author")
    slug = models.SlugField(max_length=64, verbose_name="Slug", unique=True)
    ctype = models.ForeignKey(ContentType, verbose_name="Content type")
    description = models.TextField(max_length=512, verbose_name="Description")
    website = models.URLField(verbose_name="Content website", max_length=256)
    icon = models.ImageField(verbose_name="Icon (96x96)", upload_to="downloadablecontent/icon")
    banner = models.ImageField(verbose_name="Banner", upload_to="downloadablecontent/banner")
    date = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name="Creation date")
    enabled = models.BooleanField(default=False, verbose_name="Activated")

    def __str__(self):
        return self.name


class DownloadableContentVersion(models.Model):
    version = models.CharField(max_length=64, verbose_name="Version (verbose)")
    slug = models.SlugField(max_length=64, verbose_name="Version (slug)", unique=True)
    ctype = models.ForeignKey(DownloadableContent, verbose_name="Content")
    content_url = models.URLField(verbose_name="Content URL", max_length=256)
    date = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name="Creation date")
    enabled = models.BooleanField(default=False, verbose_name="Activated")

    def __str__(self):
        return self.name