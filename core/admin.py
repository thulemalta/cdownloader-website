from django.contrib import admin
from core.models import ContentType, ContentCategory, DownloadableContent, DownloadableContentVersion
from members.models import UserMeta
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User


class VerAdmin(admin.ModelAdmin):
    list_display = ('ctype', 'slug', 'content_url')
    ordering = ('ctype', 'slug')


class CTypeAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'category')
    ordering = ('name', 'slug', 'category')


class DLAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'ctype')
    ordering = ('name', 'slug', 'ctype')


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug')
    ordering = ('name', 'slug')
    pass


class UserMetaInline(admin.StackedInline):
    model = UserMeta
    can_delete = False


class UserAdmin(UserAdmin):
    inlines = (UserMetaInline, )

admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(ContentType, CTypeAdmin)
admin.site.register(ContentCategory, CategoryAdmin)
admin.site.register(DownloadableContent, DLAdmin)
admin.site.register(DownloadableContentVersion, VerAdmin)