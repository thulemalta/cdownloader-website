# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0003_auto_20150918_1552'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usermeta',
            name='bio',
            field=models.TextField(blank=True, verbose_name='Biography', max_length=256),
        ),
        migrations.AlterField(
            model_name='usermeta',
            name='location',
            field=models.CharField(blank=True, max_length=30),
        ),
    ]
