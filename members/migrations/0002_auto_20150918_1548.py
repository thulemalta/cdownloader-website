# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('members', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='usermeta',
            name='image',
            field=models.URLField(default=''),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='usermeta',
            name='username',
            field=models.CharField(max_length=30, default='Default Username'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='usermeta',
            name='location',
            field=models.CharField(max_length=30),
        ),
    ]
