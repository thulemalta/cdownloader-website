from django.db import models
from django.contrib.auth.models import User


class UserMeta(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    username = models.CharField(max_length=30)
    image = models.URLField(max_length=256)
    location = models.CharField(max_length=30, blank=True)
    bio = models.TextField(max_length=256, verbose_name="Biography", blank=True)

