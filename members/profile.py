from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.contrib.auth.models import User
from members.models import UserMeta
from hashlib import md5


@login_required
def showprofile(request, username):
    u = User.objects.get(username=username)
    if u.usermeta.bio.strip() == "":
        u.usermeta.bio = "Nothing there !"

    if u.usermeta.location.strip() == "":
        u.usermeta.location = "Unknown location"

    return render(request, 'profile.html', {
        'u': u,
    })

@login_required
def updategravatar(request, username):
    usr = User.objects.get(username=username)
    usrmeta = UserMeta.objects.get(user=usr.id)
    m = md5()
    m.update(str(usr.email.strip()).lower().encode('utf-8'))
    usrmeta.image = 'http://www.gravatar.com/avatar/' + m.hexdigest()
    usrmeta.save(force_update=True)
    return render(request, 'text.html', {'text':'Gravatar successfully updated. (' + usrmeta.image + ')\n from email : ' + str(usr.email.strip() + " ").lower()})