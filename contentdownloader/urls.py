"""contentdownloader URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf import settings
from django.conf.urls import include, url
from django.contrib import admin

from core.views import pages, user_views
from cstore.views import summaries, view
from members import profile

admin.autodiscover()
urlpatterns = [
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT}),
    url(r'^$', pages.home),
    url(r'^login/', user_views.show_login),
    url(r'^register/', user_views.show_register),
    url(r'^logout/', user_views.show_logout),
    url(r'^already_exists/', user_views.show_already_exists),
    url(r'^thanks/', pages.thanks),
    url(r'^login_success/', pages.login_success),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^profile/view/(?P<username>[^/]+)/', profile.showprofile),
    url(r'^profile/ugravatar/(?P<username>[^/]+)/', profile.updategravatar),
    url(r'^cstore/content/(?P<content>[^/]+)/', view.showcontent),
    url(r'^cstore/contents/(?P<name>[^/]+)/', summaries.listcontents),
    url(r'^cstore/(?P<cat>[^/]+)/$', summaries.listctypes),
    url(r'^cstore/$', summaries.listcategories),
    # [a-zA-Z0-9-_]
]
