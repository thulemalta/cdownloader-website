from rest_framework import serializers
from core.models import *


class DownloadableContent(serializers.ModelSerializer):
    class Meta:
        model = DownloadableContent


class ContentType(serializers.ModelSerializer):
    class Meta:
        model = ContentType


class ContentCategory(serializers.ModelSerializer):
    class Meta:
        model = ContentCategory


class DownloadableContentVersion(serializers.ModelSerializer):
    class Meta:
        model = DownloadableContentVersion
