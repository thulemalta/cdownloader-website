from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from core.models import *

def listcategories(request):
    categories = ContentCategory.objects.filter(enabled=True).order_by('slug');
    cpaginator = Paginator(categories, 10)

    pageNumber = request.GET.get('page')

    try:
        paginatedPage = cpaginator.page(pageNumber)
    except PageNotAnInteger:
        pageNumber = 1
    except EmptyPage:
        pageNumber = cpaginator.num_pages
    categories = cpaginator.page(pageNumber)

    return render(request, 'categories.html', {
        'title': 'Content categories',
        'categories': categories,
        'page':cpaginator.page(pageNumber),
        'nextPage': int(pageNumber) + 1,
        'previousPage': int(pageNumber) - 1,
        'maxPage': cpaginator.num_pages
    })


def listctypes(request, cat):
    selected_category = ContentCategory.objects.filter(slug=cat)
    ctypes = ContentType.objects.filter(enabled=True, category=selected_category)
    cpaginator = Paginator(ctypes, 10)

    pageNumber = request.GET.get('page')

    try:
        paginatedPage = cpaginator.page(pageNumber)
    except PageNotAnInteger:
        pageNumber = 1
    except EmptyPage:
        pageNumber = cpaginator.num_pages
    ctypes = cpaginator.page(pageNumber)

    return render(request, 'categories.html', {
        'title': 'Content types',
        'categories': ctypes,
        'page':cpaginator.page(pageNumber),
        'nextPage': int(pageNumber) + 1,
        'previousPage': int(pageNumber) - 1,
        'maxPage': cpaginator.num_pages
    })


def listcontents(request, name):
    selected_ctype = ContentType.objects.filter(enabled=True, slug=name)
    contents = DownloadableContent.objects.filter(enabled=True, ctype=selected_ctype)
    cpaginator = Paginator(contents, 10)

    pageNumber = request.GET.get('page')

    try:
        paginatedPage = cpaginator.page(pageNumber)
    except PageNotAnInteger:
        pageNumber = 1
    except EmptyPage:
        pageNumber = cpaginator.num_pages
    ctypes = cpaginator.page(pageNumber)

    return render(request, 'categories.html', {
        'title': 'Contents',
        'categories': ctypes,
        'page':cpaginator.page(pageNumber),
        'nextPage': int(pageNumber) + 1,
        'previousPage': int(pageNumber) - 1,
        'maxPage': cpaginator.num_pages
    })