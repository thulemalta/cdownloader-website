from django.shortcuts import render
from core.models import *


def showcontent(request, content):
    dlcontent = DownloadableContent.objects.get(slug=content, enabled=True);

    return render(request, 'contentview.html', {
        'c': dlcontent,
    })
